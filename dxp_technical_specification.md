The S2-0011 UNI-HEAD U-X3 has the following technical specifications.

## Technical information
|Parameter|Value|
|:----|----:|
|Verfahrbereich in Z in mm|150|
|Spannung in V|24|
|Max. Stromstärke in A|4|
|Kommunikationsschnittstelle|UNICAN|

