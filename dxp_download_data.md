Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s2-0011-uni-head-u-x3).

|document|download options|
|:-----|-----:|
|operating manual           |[de](https://gitlab.com/ourplant.net/products/s2-0006-uni-head-u-x1/-/raw/main/01_operating_manual/S2-0006_F_BA_Uni%20Head.pdf); [en](https://gitlab.com/ourplant.net/products/s2-0006-uni-head-u-x1/-/raw/main/01_operating_manual/S2-0006_D_OM_Uni%20Head.pdf)|
|assembly drawing           |[de](https://gitlab.com/ourplant.net/products/s2-0011-uni-head-u-x3/-/raw/main/02_assembly_drawing/S2-0011_E_ZNB_UNI-HEAD_U-X3.pdf)|
|circuit diagram            |[de](https://gitlab.com/ourplant.net/products/s2-0011-uni-head-u-x3/-/raw/main/03_circuit_diagram/S2-0011_UNI-HEAD_U-X3.pdf)|
|maintenance instructions   |[de](https://gitlab.com/ourplant.net/products/s2-0006-uni-head-u-x1/-/raw/main/04_maintenance_instructions/S2-0006_B_WA_Uni%20Head.pdf); [en](https://gitlab.com/ourplant.net/products/s2-0006-uni-head-u-x1/-/raw/main/04_maintenance_instructions/S2-0006_A_MI_Uni%20Head.pdf)|
|spare parts                |[de](https://gitlab.com/ourplant.net/products/s2-0006-uni-head-u-x1/-/raw/main/05_spare_parts/S2-0006_C_EVL_Uni%20Head.pdf); [en](https://gitlab.com/ourplant.net/products/s2-0006-uni-head-u-x1/-/raw/main/05_spare_parts/S2-0006_C_SWP_Uni%20Head.pdf)|

<!-- 2021 (C) Häcker Automation GmbH -->
